package com.adamkorzeniak.utils.csv.parser

import com.adamkorzeniak.utils.csv.parser.test.TestModel
import spock.lang.Specification

import java.time.LocalDateTime

class CsvParserTest extends Specification {

    void testCsvRead() {
        given:
            def csvReader = CsvParser.builder()
                    .da("yyyy-MM-dd HH:mm:ss.SSS")
                    .build()
            def file = new File("src/test/resources/test.csv")

        when:
            List<TestModel> results = csvReader.read(file, TestModel.class)

        then:
            results != null
            results.size() == 2

            TestModel result1 = results[0]
            result1.string == "string1"
            result1.wrappedInteger == 1
            result1.primitiveInteger == 11
            result1.wrappedLong == 101010101010L
            result1.primitiveLong == 111111111111L
            result1.wrappedDouble == 1.0
            result1.primitiveDouble == 11.0 as double
            result1.localDateTime == LocalDateTime.of(2020, 1, 2, 3, 4, 5)
            result1.notIncluded == null

            TestModel result2 = results[1]
            result2.string == "string2"
            result2.wrappedInteger == 2
            result2.primitiveInteger == 22
            result2.wrappedLong == 202020202020L
            result2.primitiveLong == 222222222222L
            result2.wrappedDouble == 2.0
            result2.primitiveDouble == 22.0 as double
            result2.localDateTime == LocalDateTime.of(2020, 5, 4, 3, 2, 1)
            result2.notIncluded == null
    }

}
