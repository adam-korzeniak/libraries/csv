package com.adamkorzeniak.utils.csv.parser.test;

import com.adamkorzeniak.utils.csv.parser.annotation.CsvColumn;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Data
public class TestModel {

    @CsvColumn(name = "string")
    private String string;

    @CsvColumn(position = 4)
    private Integer wrappedInteger;

    @CsvColumn(name = "p_integer")
    private int primitiveInteger;

    @CsvColumn(name = "o_long")
    private Long wrappedLong;

    @CsvColumn(position = 9)
    private long primitiveLong;

    @CsvColumn(name = "big_decimal")
    private BigDecimal bigDecimal;

    @CsvColumn(name = "o_double")
    private Double wrappedDouble;

    @CsvColumn(name = "p_double")
    private double primitiveDouble;

    @CsvColumn(name = "local_date_time")
    private LocalDateTime localDateTime;

    private OffsetDateTime notIncluded;

}
