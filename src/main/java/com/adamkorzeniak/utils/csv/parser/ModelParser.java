package com.adamkorzeniak.utils.csv.parser;

import com.adamkorzeniak.utils.csv.parser.annotation.CsvColumn;
import com.adamkorzeniak.utils.csv.parser.exception.CsvProcessingException;
import com.opencsv.exceptions.CsvValidationException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class ModelParser {
    private final CsvReaderConfig csvReaderConfig;

    <T> ModelRepresentation<T> getModelRepresentation(CsvInput<T> csvInput) throws IOException, CsvValidationException, CsvProcessingException {
        List<String> headers = List.of();
        if (csvReaderConfig.headersIncluded()) {
            headers = Arrays.asList(csvInput.getReader().readNext());
        }
        return buildRepresentation(csvInput, headers);
    }

    private static <T> ModelRepresentation<T> buildRepresentation(CsvInput<T> csvInput, List<String> headers) throws CsvProcessingException {
        List<FieldDetails> fieldDetails = new ArrayList<>();
        for (Field field : getAnnotatedInstanceFields(csvInput)) {
            fieldDetails.add(buildFieldDetails(field, headers));
        }
        return new ModelRepresentation<T>(csvInput.getClazz(), fieldDetails);
    }

    private static <T> List<Field> getAnnotatedInstanceFields(CsvInput<T> csvInput) {
        return ReflectionUtils.getCsvColumnFields(csvInput.getClazz()).toList();
    }

    private static FieldDetails buildFieldDetails(Field field, List<String> headers) throws CsvProcessingException {
        int columnPosition = getColumnPosition(field, headers);
        return new FieldDetails(field.getName(), field.getType(), columnPosition);
    }

    private static int getColumnPosition(Field field, List<String> headers) throws CsvProcessingException {
        String columnName = getCsvColumnName(field);
        int columnPosition = getCsvColumnPosition(field);
        if (isValidCombination(columnName, columnPosition)) {
            if (columnPosition < 0) {
                columnPosition = headers.indexOf(columnName);
                if (columnPosition < 0) {
                    throw new RuntimeException(MessageFormat.format("Column {0} not found", columnName));
                }
            }
        } else {
            throw new CsvProcessingException("You need to provide one of csv column name or position");
        }
        return columnPosition;
    }

    private static boolean isValidCombination(String columnName, int columnPosition) {
        return StringUtils.isBlank(columnName) && columnPosition >= 0
                || StringUtils.isNotBlank(columnName) && columnPosition < 0;
    }

    private static String getCsvColumnName(Field field) {
        CsvColumn annotation = field.getAnnotation(CsvColumn.class);
        return annotation.name();
    }

    private static int getCsvColumnPosition(Field field) {
        CsvColumn annotation = field.getAnnotation(CsvColumn.class);
        return annotation.position();
    }
}
