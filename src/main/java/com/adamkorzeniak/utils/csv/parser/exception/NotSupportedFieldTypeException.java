package com.adamkorzeniak.utils.csv.parser.exception;

import java.util.Set;

public class NotSupportedFieldTypeException extends CsvProcessingException {
    private static final String MESSAGE = "Class %s, contains unsupported field types: %s";

    public NotSupportedFieldTypeException(Class<?> clazz, Set<Class<?>> unsupportedFieldTypes) {
        super(String.format(MESSAGE, clazz, unsupportedFieldTypes));
    }
}
