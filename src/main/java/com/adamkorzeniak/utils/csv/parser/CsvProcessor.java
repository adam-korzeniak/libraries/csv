package com.adamkorzeniak.utils.csv.parser;

import com.adamkorzeniak.utils.csv.parser.exception.CsvProcessingException;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
class CsvProcessor {
    private final ObjectRepresentationBuilder objectRepresentationBuilder;
    private final ModelParser modelParser;

    CsvProcessor(ContentConvertions contentConvertions, CsvReaderConfig config) {
        this.objectRepresentationBuilder = new ObjectRepresentationBuilder(contentConvertions);
        this.modelParser = new ModelParser(config);
    }

    <T> List<T> read(CsvInput<T> input) throws IOException, CsvValidationException, CsvProcessingException {
        ModelRepresentation<T> representation = modelParser.getModelRepresentation(input);
        return getRecords(input.getReader(), representation);
    }

    private <T> List<T> getRecords(CSVReader csvReader, ModelRepresentation<T> representation) throws CsvValidationException, IOException, CsvProcessingException {
        List<T> records = new ArrayList<>();
        String[] values;
        while ((values = csvReader.readNext()) != null) {
            records.add(objectRepresentationBuilder.buildRecord(representation, values));
        }
        return records;
    }
}
