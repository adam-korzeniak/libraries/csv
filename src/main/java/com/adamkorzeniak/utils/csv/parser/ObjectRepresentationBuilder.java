package com.adamkorzeniak.utils.csv.parser;

import com.adamkorzeniak.utils.csv.parser.annotation.CsvColumn;
import com.adamkorzeniak.utils.csv.parser.exception.CsvProcessingException;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.util.Optional;

@RequiredArgsConstructor
class ObjectRepresentationBuilder {

    private final ContentConvertions contentConvertions;

    <T> T buildRecord(ModelRepresentation<T> modelRepresentation, String[] values) throws CsvProcessingException {
        T object = initializeObject(modelRepresentation);
        for (FieldDetails fieldDetails : modelRepresentation.fieldDetails()) {
            try {
                Field field = object.getClass().getDeclaredField(fieldDetails.name());
                Class<?> fieldClass = fieldDetails.clazz();
                String value = values[fieldDetails.columnPosition()];
                setField(object, field, fieldClass, value);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException("Field doesnt exist");
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Field access invalid");
            }
        }
        return object;
    }

    private <T> T initializeObject(ModelRepresentation<T> representation) throws CsvProcessingException {
        try {
            Class<T> clazz = representation.clazz();
            Constructor<T> ctor = clazz.getConstructor();
            return ctor.newInstance();
        } catch (NoSuchMethodException e) {
            throw new CsvProcessingException("Class has to provide default constructor", e);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new CsvProcessingException("Failed to instantiate class", e);
        }
    }

    private <T> void setField(T object, Field field, Class<?> fieldClass, String value) throws IllegalAccessException {
        field.setAccessible(true);
        Object result = contentConvertions.getMappingFunction(fieldClass)
                .apply(value, getParams(field, fieldClass));
        field.set(object, result);
    }

    private static ConvertionParams getParams(Field field, Class<?> fieldClass) {
        return Optional.of(field)
                .filter(f -> fieldClass.equals(LocalDateTime.class))
                .flatMap(ObjectRepresentationBuilder::getDateTimeFormatter)
                .map(ConvertionParams::ofDateFormat)
                .orElseGet(ConvertionParams::empty);
    }

    private static Optional<String> getDateTimeFormatter(Field field) {
        return Optional.of(field.getAnnotation(CsvColumn.class))
                .map(CsvColumn::dateTimeFormatter)
                .filter(value -> !value.isBlank());
    }
}
