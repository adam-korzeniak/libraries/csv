package com.adamkorzeniak.utils.csv.parser;

import com.opencsv.CSVReader;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
class CsvInput<T> {
    private final CSVReader reader;
    private final Class<T> clazz;
}
