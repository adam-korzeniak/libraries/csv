package com.adamkorzeniak.utils.csv.parser.exception;

//TODO: Improve exceptions, should I use checked?
public class CsvProcessingException extends Exception {

    public CsvProcessingException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public CsvProcessingException(String message) {
        super(message);
    }
}
