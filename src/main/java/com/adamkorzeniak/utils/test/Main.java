package com.adamkorzeniak.utils.test;

import com.adamkorzeniak.utils.csv.parser.CsvParser;
import com.adamkorzeniak.utils.csv.parser.exception.CsvProcessingException;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static final String PATH = "xxx.csv";

    public static void main(String[] args) throws URISyntaxException {
        CsvParser csvParser = CsvParser.builder().build();
        Path path = Paths.get(ClassLoader.getSystemResource(PATH).toURI());
        File file = path.toFile();
        try {
            List<PedometerDailySummary> result = csvParser.read(file, PedometerDailySummary.class);
            System.out.println(result);
        } catch (CsvProcessingException e) {
            e.printStackTrace();
        }
    }
}
