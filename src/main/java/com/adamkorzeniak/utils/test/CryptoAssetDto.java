package com.adamkorzeniak.utils.test;

import com.adamkorzeniak.utils.csv.parser.annotation.CsvColumn;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.math.BigDecimal;

@Data
public final class CryptoAssetDto {
    @JsonIgnore
    private Long id;

    @CsvColumn(name = "Nazwa")
    private String name;
    @CsvColumn(name = "Symbol")
    private String symbol;
    @CsvColumn(name = "Cmc Id")
    private String externalId;
    @CsvColumn(name = "Ilość")
    private BigDecimal amount;
    private String location;


}
