package com.adamkorzeniak.utils.test;

import com.adamkorzeniak.utils.csv.parser.CsvParser;
import com.adamkorzeniak.utils.csv.parser.exception.CsvProcessingException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Main2 {

    public static final String PATH = "/home/adam/Home/Dane/crypto.csv";

    public static void main(String[] args) {
        CsvParser csvParser = CsvParser.builder().build();
        Path path = Paths.get(PATH);
        File file = path.toFile();
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<CryptoAssetDto> result = csvParser.read(file, CryptoAssetDto.class);
            System.out.println(mapper.writeValueAsString(result));
        } catch (CsvProcessingException | JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
